CREATE TABLE IF NOT EXISTS `civicrm_turfcutter` (
       `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Turfcutter ID',
       `datetime` datetime DEFAULT NULL COMMENT 'Date and time that entry was added to database',
       `serialized_turf` text COLLATE utf8_unicode_ci COMMENT 'The serialized turf information',
       `survey_title` varchar(255) COLLATE utf8_unicode_ci COMMENT 'Title of Survey matched with turf',
       `volunteer_id` int(10) unsigned NOT NULL COMMENT 'The civicrm id of the volunteer',
       `volunteer_name` varchar(255) COLLATE utf8_unicode_ci COMMENT 'Name of volunteer being assigned to turf',
       PRIMARY KEY(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
