function populate_volunteer_list(){
  var volunteer_list = document.getElementById("volunteer_info_volunteer_list");
  volunteer_list.className += "fa-ul";
  var colors = new marker_colors();
  var shapes = new marker_shape();
  CRM.$.ajaxSetup({async: false}); //NOTE TO SELF! Get rid of this line and replace with ajax promises
  CRM.api3('Contact', 'get', {
    "sequential": 1,
    "contact_type": "Individual",
    "contact_sub_type": "Volunteer"
  }).done(function(result) {
    for(var i = 0; i<result.values.length; i++){
      if(result.values[i].first_name == "" && result.values[i].last_name==""){
        continue; // if they don't have a first and last name skip
      }
      var element1 = document.createElement("li");
      element1.style.display = "list-item";
      element1.style.listStyleType = "";
      element1.className = shapes.nextShape();
      var element1Child = document.createElement("a");
      element1Child.href = "#" + i;
      element1Child.style.fontSize = "large";
      element1Child.text = result.values[i].first_name + " " + result.values[i].last_name;
      element1Child.title = result.values[i].id;
      element1Child.style.color = colors.nextColor();
      element1Child.onclick = volunteer_list_element_onClick;
      element1Child.onmouseover = volunteer_list_element_onmouseover;
      element1Child.onmouseout = volunteer_list_element_onmouseout;

      element1.appendChild(element1Child);
      element1.className += " volunteerinfo_volunteer_name";
      volunteer_list.appendChild(element1);
    }
});
}

function volunteer_list_element_onClick(){
  var canvasserID = this.title; //Note that we are using the title for temporary storage of the id of the contact
  // this.style.color = 'red';
  var volunteer_color = this.style.color;
  var volunteer_icon = this.parentElement.classList[1];
  CRM.api3('Survey', 'selectbyvolunteername', {
    "volunteer_id": canvasserID
  }).done(function(result) {
    // itterate through all of the markers already out and change their icon to the volunteer icon
    var idList = [];
    for(var i =0; i < result.count; i++){
      idList.push(result.values[i].id);
    }
    var icon = volunteer_icon;
    var iconColor = "white";
    var markerColor = volunteer_color;
    var shape = "square";
    voters.eachLayer(function(layer){
      if(idList.includes(layer.ID)){
        var options = layer.options.icon.options;
        if(options.icon == icon && options.iconColor == iconColor && options.markerColor && markerColor && options.shape == shape ){
          //Case where it is already changed, switch back to default
          var defaultIcon = L.ExtraMarkers.icon({
            icon : "fa-circle",
            markerColor : "blue",
            iconColor : "white",
            shape : "circle",
            prefix : "fa"
          });
          layer.setIcon(layer.options.icon = defaultIcon);
        }
        else{
          // Switch to the volunteers color and shape
          var newIcon = L.ExtraMarkers.icon({
            icon: icon,
            iconColor: iconColor,
            markerColor: markerColor,
            shape: shape,
            prefix: 'fa'
          });
          layer.setIcon(layer.options.icon = newIcon);
        }
      }
    });
  });
}

function volunteer_list_element_onmouseover(){
  var canvasserID = this.title;
  var volunteer_color = this.style.color;
  var volunteer_icon = this.parentElement.classList[1];
  CRM.api3('Survey', 'selectbyvolunteername', {
    "volunteer_id" : canvasserID
  }).done(function(result){
    var listOfMarkers = [];
    for(var i=0; i < result.count; i++){
      var location = [result.values[i].geo_code_1,result.values[i].geo_code_2];
      var redMarker = L.ExtraMarkers.icon({
        icon: volunteer_icon,
        iconColor: "white",
        markerColor: volunteer_color,
        shape: 'circle',
        prefix: 'fa'
      });
      var marker = L.marker(location, {icon : redMarker}).bindPopup("<div style='text-align: center; margin-left: auto; margin-right: auto;'>"+ result.values[i].first_name + " " + result.values[i].last_name +"</div>", {maxWidth: '400'});
      listOfMarkers.push(marker);
    }
    var currentLayerGroup = L.layerGroup(listOfMarkers);
    window.currentLayerGroup = currentLayerGroup;
    map.addLayer(currentLayerGroup);
  });
}

function volunteer_list_element_onmouseout(){
  var currentLayerGroup = window.currentLayerGroup;
  delete window.currentLayerGroup;
  map.removeLayer(currentLayerGroup);
}

function marker_colors(){
  this.Colors = ['red', 'orange-dark', 'orange', 'yellow', 'blue-dark', 'cyan', 'purple', 'violet', 'pink', 'green-dark', 'green', 'green-light'];
  this.nextColor = function(){
    var toReturn = this.Colors.shift();
    this.Colors.push(toReturn);
    return toReturn;
  };
}

function marker_shape(){
  this.Shapes = ["fa fa-circle", "fa fa-caret-up", "fa fa-square", "fa fa-arrows-alt","fa fa-anchor","fa fa-angle-double-up"];
  this.nextShape = function(){
    var toReturn = this.Shapes.shift();
    this.Shapes.push(toReturn);
    return toReturn;
  };
}
