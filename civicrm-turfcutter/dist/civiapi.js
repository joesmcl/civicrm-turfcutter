function get_voter_markers_from_civi(){
  var listOfMarkers = [];
  CRM.$.ajaxSetup({async : false});
  CRM.api3('Contact', 'get', {
    "sequential": 1,
    "return": ["geo_code_1","geo_code_2","first_name","last_name","id"],
    "contact_type": "Individual",
    "options": {"limit":1000}
  }).done(function(result) {
    for(var i=0; i<result.count; i++){
      var location = [result.values[i].geo_code_1,result.values[i].geo_code_2];
      var redMarker = L.ExtraMarkers.icon({
        icon: 'fa-circle',
        iconColor: "white",
        markerColor: 'blue',
        shape: 'circle',
        prefix: 'fa'
      });
      var marker = L.marker(location, {
        "id" : result.values[i].id,
        icon : redMarker
      });
      marker.bindPopup("<div style='text-align: center; margin-left: auto; margin-right: auto;'>"+ result.values[i].first_name + " " + result.values[i].last_name +"</div>", {maxWidth: '400'});
      marker.ID = result.values[i].id;
      listOfMarkers.push(marker);
    }
  });
  return L.layerGroup(listOfMarkers);
}

function get_volunteer_markers_from_civi(){
  var listOfMarkers = [];
  CRM.$.ajaxSetup({async : false});
  CRM.api3('Contact', 'get', {
    "sequential": 1,
    "return": ["geo_code_1","geo_code_2","first_name","last_name"],
    "contact_type": "Individual",
    "options": {"limit":1000},
    "contact_sub_type": "Volunteer"
  }).done(function(result) {
    // do something
    for(var i=0; i<result.count; i++){
      var location = [result.values[i].geo_code_1,result.values[i].geo_code_2];
      var redMarker = L.ExtraMarkers.icon({
        // icon: 'fa-coffee',
        markerColor: 'red',
        shape: 'square',
        // prefix: 'fa'
      });
      var marker = L.marker(location,{
        icon: redMarker
      });
      listOfMarkers.push(marker.bindPopup("<div style='text-align: center; margin-left: auto; margin-right: auto;'>"+ result.values[i].first_name + " " + result.values[i].last_name +"</div>", {maxWidth: '400'}));
    }
  });
  return L.layerGroup(listOfMarkers);
}

function add_voters_to_survey(surveyID,interviewerID,respondentID){
  console.log(respondentID, "the respondents");
  var date = new Date();
  var new_group_name_string = "contact_group_for_survey_" + surveyID + "_with_interviewerID_" + interviewerID + date.toDateString();
  CRM.$.ajaxSetup({async : false});
  CRM.api3('SurveyRespondant', 'create', {
    "survey_id": surveyID,
    "interviewer_id": interviewerID,
    "respondent_ids": respondentID,
    "new_group_name" : new_group_name_string
  }).done(function(result) {
    // do something
    console.log("tried to add voters to survey");
    console.log(result);
  });
}
