function get_all_volunteers(){
    CRM.api3('Contact', 'get', {
        "sequential": 1,
        "contact_type": "Individual",
        "contact_sub_type": "Volunteer",
        "return": ["id","geo_code_1","geo_code_2","street_address","postal_code","city","state_province","preferred_language","legal_name","nick_name"]
    }).done(function(data) {
        for(var i=0; i<data.length; i++){
            var location = new L.latlng(data[i].geo_code_1,data[i].geo_code_2);
            var name = data[i].legal_name;
            var nick_name = data[i].nick_name;
            var language = data[i].preferred_language;
            var id = data[i].id;
            var address = data[i].street_address;
            var zipcode = data[i].postal_code;
            var city = data[i].city;
            var state = data[i].state_province;


            var marker = new L.marker(location, {
                title: name
            });
            marker.bindPopup("<div style='text-align: center; margin-left: auto; margin-right: auto;'>"+ title + city +"</div>", {maxWidth: '400'});
            marker.addTo(map);
        }
    });

}

function get_volunteers_by_parameters(language="en_US", latitude_lower_bound=-1000, latitude_upper_bound=1000, longitude_lower_bound=-1000, longitude_upper_bound=1000){
    CRM.api3('Contact', 'get', {
        "sequential": 1,
        "contact_type": "Individual",
        "contact_sub_type": "Volunteer",
        "return": ["id","geo_code_1","geo_code_2","street_address","postal_code","city","state_province","preferred_language","legal_name","nick_name"],
        "geo_code_1": {"BETWEEN":[latitude_lower_bound,latitude_upper_bound]},
        "geo_code_2": {"BETWEEN":[longitude_lower_bound,longitude_upper_bound]},
        "postal_code": {"<=":100000},
        "preferred_language": language
    }).done(function(data) {
        for(var i=0; i<data.length;i++){
            var location = new L.latlng(data[i].geo_code_1,data[i].geo_code_2);
            var name = data[i].legal_name;
            var nick_name = data[i].nick_name;
            var language = data[i].preferred_language;
            var id = data[i].id;
            var address = data[i].street_address;
            var zipcode = data[i].postal_code;
            var city = data[i].city;
            var state = data[i].state_province;

            var marker = new L.marker(location, {
                title : name
            });
            marker.bindPopup("<div style='text-align: center; margin-left: auto; margin-right: auto;'>"+ title + city +"</div>", {maxWidth: '400'});
            marker.addTo(map);
        }
    });

}

function get_volunteers_in_box(latitude_lower_bound=-1000,latitude_upper_bound=1000,longitude_lower_bound=-1000,longitude_upper_bound=1000){
    CRM.api3('Contact', 'get', {
        "sequential": 1,
        "contact_type": "Individual",
        "contact_sub_type": "Volunteer",
        "return": ["id","geo_code_1","geo_code_2","street_address","postal_code","city","state_province","preferred_language","legal_name","nick_name"],
        "geo_code_1": {"BETWEEN":[latitude_lower_bound,latitude_upper_bound]},
        "geo_code_2": {"BETWEEN":[longitude_lower_bound,longitude_upper_bound]}
    }).done(function(data) {
        for(var i=0;i<data.length;i++){
            var location = new L.latlng(data[i].geo_code_1,data[i].geo_code_2);
            var name = data[i].legal_name;
            var nick_name = data[i].nick_name;
            var language = data[i].preferred_language;
            var id = data[i].id;
            var address = data[i].street_address;
            var zipcode = data[i].postal_code;
            var city = data[i].city;
            var state = data[i].state_province;

            var marker = new L.marker(location, {
                title : name
            });
            marker.bindPopup("<div style='text-align: center; margin-left: auto; margin-right: auto;'>"+ title + city +"</div>", {maxWidth: '400'});
            marker.addTo(map);
        }
    });

}

// function volunteer_place_of_work(volunteerID){
// }

