function generate_language_options(){
    var myDiv = document.getElementById("language_list");

    //Create array of options to be added
    var array = ["Volvo","Saab","Mercades","Audi"];

    //Create and append select list
    var selectList = document.createElement("select");
    selectList.multiple = "multiple";
    selectList.className = "listy";
    selectList.setAttribute("id", "language_select");
    myDiv.appendChild(selectList);

    //Create and append the options
    for (var i = 0; i < array.length; i++) {
        var option = document.createElement("option");
        option.setAttribute("value", array[i]);
        option.text = array[i];
        selectList.appendChild(option);
    }
}

function submit_button_onClick(){
    var sliderDivId = "people_not_contacted_in_last_x_days_slider";
    var languageListDivId = "language_select";
    var people_not_contacted_slider = document.getElementById(sliderDivId);
    var language_list = document.getElementById(languageListDivId);
    console.log(people_not_contacted_slider.value);

   // This section handles reading values from a list
    for(var i=0; i< language_list.options.length; i++){
        if(language_list.options[i].selected){
            console.log(language_list.options[i].value);
        }
    }
}

function change(){
    var result = document.getElementById("result");
    var mine = document.getElementById("people_not_contacted_in_last_x_days_slider");
    result.innerText = mine.value;
}
