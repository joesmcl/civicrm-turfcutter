function volunteerStringGeneration(){
  var volunteerReturnString = '<select class="form-control" id="submit_form_volunteer" name="volunteer">';
  CRM.$.ajaxSetup({async:false});
  CRM.api3('Contact', 'get', {
    "sequential": 1,
    "contact_sub_type": "Volunteer"
  }).done(function(result) {
    // do something
    for(var i=0; i < result.count; i++){
      volunteerReturnString += '<option value="' + result.values[i].first_name + " " + result.values[i].last_name + '"' + 'data-id=' + result.values[i].id + '>' + result.values[i].first_name + " " + result.values[i].last_name +'</option>';
    }
  });
  return volunteerReturnString + '</select>';
}

function languageStringGeneration(){
  var languageReturnString = '<select class="form-control" id="submit_form_language" name="language">';
  var languageset = [];
  CRM.api3('Contact', 'get', {
    "sequential": 1,
    "return": ["preferred_language"],
    "contact_type": "Individual",
    "options": {"limit":1000000}
  }).done(function(result) {
    // do something
    for(var i=0; i< result.count; i++){
      if(languageset.includes(result.values[i].preferred_language)){
        continue;
      }
      else{
        if(result.values[i].preferred_language===""){
          languageset.push("");
          continue;
        }
        else{
          languageReturnString +='<option value="' + result.values[i].preferred_language +'">' + result.values[i].preferred_language + '</option>';
          languageset.push(result.values[i].preferred_language);
        }
      }
    }
  });
  return languageReturnString + '</select>';
}

function timeAssignedForStringGeneration(){
    return '<input type="date" placeholder="Required" class="form-control" id="submit_form_date" name="submit_form_date">';
}

function generateAvailableSurveys(){
  var stringForReturn = '<select class="form-control" id="submit_existing_survey" name="existing_survey">'+
      '<option value="None">No</option>';
  CRM.$.ajaxSetup({async: false}); //NOTE TO SELF! Get rid of this line and replace with ajax promises
  CRM.api3('Survey', 'get', {
    "sequential": 1
  }).done(function(result) {
    for(var i = 0; i<result.values.length; i++){
      stringForReturn += '<option value="' + result.values[i].title + '" data-id = "' + result.values[i].id + '"  >'+ result.values[i].title + '</option>';
    }
  });
  return stringForReturn + '</select>';
}

function get_popupcontentPolygon(latlng,voterIDsWithinPolygon){
  window.voterIDsWithinPolygon = voterIDsWithinPolygon; //Here we use a global variable for the namespace of the html
    var popupcontentPolygon =  '<form role="form" id="form" enctype="multipart/form-data" class = "form-horizontal" method="post">'+
    '<div class="form-group">'+
    '<label class="control-label col-sm-5"><strong>Volunteer: </strong></label>'+
    volunteerStringGeneration() +
    '</div>'+
    '<div class="form-group">'+
    '<label class="control-label col-sm-5"><strong>Language: </strong></label>'+
    languageStringGeneration() +
    '</div>'+
    '<div class="form-group">'+
    '<label class="control-label col-sm-5"><strong>Time assigned for: </strong></label>'+
    timeAssignedForStringGeneration()
    +
    '</div>'+
    '<div class="form-group">' +
    '<label class="control-label col-sm-5"><strong> Use Existing Survey: </strong></label>'+
        generateAvailableSurveys() +
    '</div>' +
    '<div class="form-group">'+
    '<label class="control-label col-sm-5"><strong>Description: </strong></label>'+
    '<textarea class="form-control" rows="6" id="descrip" name="descript">...</textarea>'+
    '</div>'+
    '<input style="display: none;" type="text" id="lat" name="lat" value="'+latlng.lat.toFixed(6)+'" />'+
    '<input style="display: none;" type="text" id="lng" name="lng" value="'+latlng.lng.toFixed(6)+'" />'+
    '<div class="form-group">'+
    '<div style="text-align:center;" class="col-xs-4"><button type="submit" value="submit" class="btn btn-primary" onclick="survey_handler_for_polygon(this)" id="submit_polygon_form">Submit</button></div>'+
        '</div>'+ '</form>';
    return popupcontentPolygon;
}

function survey_handler_for_polygon(element){
  var voterIDsWithinPolygon = window.voterIDsWithinPolygon;
  delete window.voterIDsWithinPolygon;
  var volunteerValue = document.getElementById("submit_form_volunteer").value;
  var volunteerID = document.getElementById("submit_form_volunteer").lastChild.dataset.id;
    var languageValue = document.getElementById("submit_form_language").value;
    var dateValue = document.getElementById("submit_form_date").value;
  var surveytitle = document.getElementById("submit_existing_survey").value;
  var surveyID = document.getElementById("submit_existing_survey").lastChild.dataset.id;
  console.log(surveyID);
  add_voters_to_survey(surveyID,volunteerID,voterIDsWithinPolygon);
}

function get_popupcontentRectangle(centerlatlng){
    var popupcontentRectangle =  '<form role="form" id="form" enctype="multipart/form-data" class = "form-horizontal" method="post">'+
    '<div class="form-group">'+
    '<label class="control-label col-sm-5"><strong>Volunteer: </strong></label>'+
    volunteerStringGeneration() +
    '</div>'+
    '<div class="form-group">'+
    '<label class="control-label col-sm-5"><strong>Language: </strong></label>'+
    languageStringGeneration() +
    '</div>'+
    '<div class="form-group">'+
    '<label class="control-label col-sm-5"><strong>Time assigned for: </strong></label>'+
    timeAssignedForStringGeneration() +
        '</div>'+
        '<div class="form-group">' +
        '<label class="control-label col-sm-5"><strong> Use Existing Survey: </strong></label>'+
        generateAvailableSurveys() +
        '</div>' +
    '<div class="form-group">'+
    '<label class="control-label col-sm-5"><strong>Description: </strong></label>'+
    '<textarea class="form-control" rows="6" id="descrip" name="descript">...</textarea>'+
    '</div>'+
    '<input style="display: none;" type="text" id="lat" name="lat" value="'+centerlatlng.lat.toFixed(6)+'" />'+
    '<input style="display: none;" type="text" id="lng" name="lng" value="'+centerlatlng.lng.toFixed(6)+'" />'+
    '<div class="form-group">'+
    '<div style="text-align:center;" class="col-xs-4"><button type="submit" value="submit" class="btn btn-primary" onclick="ajax_request_for_rectangle(this)" id="submit_polygon_form">Submit</button></div>'+
    '</div>'+ '</form>';
    return popupcontentRectangle;
}

function ajax_request_for_rectangle(element){
  var volunteerValue = document.getElementById("submit_form_volunteer").value;
  var languageValue = document.getElementById("submit_form_language").value;
  var dateValue = document.getElementById("submit_form_date").value;
  var surveyValue = document.getElementById("submit_existing_survey").value;
  console.log(surveyValue);
  if(surveyValue == "None"){
    //This is the case where we handle the end user generating their own survey
    console.log("End user is choosing to make their own survey");
  }
}

function get_popupcontentCircle(centerlatlng){
    var popupcontentCircle =  '<form role="form" id="form" enctype="multipart/form-data" class = "form-horizontal" method="post">'+
    '<div class="form-group">'+
    '<label class="control-label col-sm-5"><strong>Volunteer: </strong></label>'+
    volunteerStringGeneration() +
    '</div>'+
    '<div class="form-group">'+
    '<label class="control-label col-sm-5"><strong>Language: </strong></label>'+
    languageStringGeneration() +
    '</div>'+
    '<div class="form-group">'+
    '<label class="control-label col-sm-5"><strong>Time assigned for: </strong></label>'+
    timeAssignedForStringGeneration() +
        '</div>'+
        '<div class="form-group">' +
        '<label class="control-label col-sm-5"><strong> Use Existing Survey: </strong></label>'+
        generateAvailableSurveys() +
        '</div>' +
    '<div class="form-group">'+
    '<label class="control-label col-sm-5"><strong>Description: </strong></label>'+
    '<textarea class="form-control" rows="6" id="descrip" name="descript">...</textarea>'+
    '</div>'+
    '<input style="display: none;" type="text" id="lat" name="lat" value="'+centerlatlng.lat.toFixed(6)+'" />'+
    '<input style="display: none;" type="text" id="lng" name="lng" value="'+centerlatlng.lng.toFixed(6)+'" />'+
    '<div class="form-group">'+
    '<div style="text-align:center;" class="col-xs-4"><button type="submit" value="submit" class="btn btn-primary" onclick="ajax_request_for_circle(this)" id="submit_polygon_form">Submit</button></div>'+
    '</div>'+ '</form>';
    return popupcontentCircle;

}

function ajax_request_for_circle(element){
    console.log("This is circle ");
}
