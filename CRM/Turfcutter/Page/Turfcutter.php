<?php
use CRM_Turfcutter_ExtensionUtil as E;

// class CRM_Turfcutter_Page_Turfcutter {

class CRM_Turfcutter_Page_Turfcutter extends CRM_Core_Page {
  public function run() {
    // Example: Set the page-title dynamically; alternatively, declare a static title in xml/Menu/*.xml
    CRM_Utils_System::setTitle(E::ts('Turfcutter'));

    // Example: Assign a variable for use in a template
    $this->assign('currentTime', date('Y-m-d H:i:s'));
    // include('../../../civicrm-turfcutter/dist/index.html');
    Civi::resources()->addStyleUrl("https://unpkg.com/leaflet@1.2.0/dist/leaflet.css",0,'html-header');
    Civi::resources()->addScriptUrl("https://unpkg.com/leaflet@1.0.3/dist/leaflet.js",0,'html-header');
    Civi::resources()->addScriptFile('com.asludds.turfcutter','civicrm-turfcutter/dist/leaflet.extra-markers.min.js',1,'html-header');
    Civi::resources()->addStyleFile('com.asludds.turfcutter','civicrm-turfcutter/dist/css/leaflet.extra-markers.min.css',1,'html-header');
    Civi::resources()->addScriptFile('com.asludds.turfcutter', 'civicrm-turfcutter/node_modules/leaflet-draw/dist/leaflet.draw.js', 1, 'html-header');
    Civi::resources()->addStyleFile('com.asludds.turfcutter', 'civicrm-turfcutter/node_modules/leaflet-draw/dist/leaflet.draw.css', 1, 'html-header');
    Civi::resources()->addScriptFile('com.asludds.turfcutter', 'civicrm-turfcutter/node_modules/leaflet-sidebar-v2/js/leaflet-sidebar.js', 2, 'html-header');
    Civi::resources()->addStyleFile('com.asludds.turfcutter', 'civicrm-turfcutter/node_modules/leaflet-sidebar-v2/css/leaflet-sidebar.css', 1, 'html-header');
    Civi::resources()->addScriptFile('com.asludds.turfcutter', 'civicrm-turfcutter/dist/drawcontrol.js', 1, 'html-header');
    Civi::resources()->addScriptFile('com.asludds.turfcutter', 'civicrm-turfcutter/node_modules/@turf/turf/turf.min.js', 1, 'html-header');
    Civi::resources()->addScriptFile('com.asludds.turfcutter', 'civicrm-turfcutter/dist/popupcontent.js', 1, 'html-header');
    Civi::resources()->addStyleFile('com.asludds.turfcutter', 'civicrm-turfcutter/dist/css/heatmap.css', 1, 'html-header');
    Civi::resources()->addScriptFile('com.asludds.turfcutter', 'civicrm-turfcutter/dist/heatmap.js', 1, 'html-header');
    Civi::resources()->addScriptFile('com.asludds.turfcutter', 'civicrm-turfcutter/dist/civiapi.js', 1, 'html-header');
    Civi::resources()->addScriptFile('com.asludds.turfcutter', 'civicrm-turfcutter/dist/volunteerinfo.js', 1, 'html-header');
    Civi::resources()->addStyleFile('com.asludds.turfcutter', 'civicrm-turfcutter/dist/volunteerinfo.css', 1, 'html-header');
    Civi::resources()->addScriptUrl('com.asludds.turfcutter',"http://code.jquery.com/jquery-2.1.1.min.js",1,'html-header');
    Civi::resources()->addScriptFile('com.asludds.turfcutter', 'civicrm-turfcutter/dist/extendControlLayer.js', 1, 'html-header');
    // Civi::resources()->addStyleURL('com.asludds.turfcutter',
    // "https://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css",
    // 1, 'html-header');
    Civi::resources()->addStyleURL('com.asudds.turfcutter','https://use.fontawesome.com/releases/v5.1.0/css/all.css',1,'html-header');
    Civi::resources()->addStyleURL('com.asludds.turfcutter',"https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css",1,'html-header');
    Civi::resources()->addScriptFile('com.asludds.turfcutter','civicrm-turfcutter/dist/get_voters.js',1,'html-header');


    // echo getcwd();
    include('sites/default/files/civicrm/ext/com.asludds.turfcutter/civicrm-turfcutter/dist/index.html');
    // include('sites/default/files/civicrm/ext/com.asludds.turfcutter/civicrm-turfcutter/test.html');

    parent::run();
  }

}
?>
